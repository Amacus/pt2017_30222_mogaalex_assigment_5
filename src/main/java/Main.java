import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Calendar.DAY_OF_YEAR;
import static java.util.Map.Entry;
import static java.util.stream.Collectors.groupingBy;

/**
 * Created by Amacus5 on 20.05.2017.
 */
public class Main {
    public static int sum(Map<String, Integer> m){
        int suma = m.values().stream().mapToInt(Number::intValue).sum();

        return suma;

    }
    public static void main(String[] args) {
        List<MonitoredData> dates = new ArrayList<MonitoredData>();
        Text text = new Text();
        text.getText(dates);
        long result1;
        result1 = dates.stream()
                .map(m -> m.getStart().get(DAY_OF_YEAR))
                .distinct()
                .count();
        System.out.println("resul1: " + result1);

        Map<String, Integer> result2 = null;
        result2 = dates.stream()
                .collect(groupingBy(MonitoredData::getActivity, Collectors.summingInt(a->1)));
        result2.forEach((k, v) -> text.setText(k + ", " + v.toString()));
        //for (Map.Entry entry : result2.entrySet()) {
        //System.out.println(entry.getKey() + ", " + entry.getValue());
        //}
        Map<Integer, Map<String, Integer>> result3 = null;
        result3 = dates.stream()
                .collect(groupingBy((MonitoredData m) -> m.getStart().get(DAY_OF_YEAR),
                        groupingBy(MonitoredData::getActivity, Collectors.summingInt(a -> 1))));
        for (Entry entry : result3.entrySet()) {
        System.out.println(entry.getKey() + ", " + entry.getValue());
    }
        Map<String, Long> result4 = null;
        Map<String, Long> result14 = null;
        result4=dates.stream()
                .collect(Collectors.toMap(MonitoredData::getActivity, a-> (((ChronoUnit.HOURS.between(a.getStart().toInstant(),a.getEnd().toInstant())))),(p1, p2) -> p1+p2));
        result14=result4.entrySet().stream().filter(p->p.getValue()>10).collect(Collectors.toMap(Entry::getKey, Entry::getValue));
        result14.forEach((k, v) -> text.setText(k + ", " + v.toString()));
        for (Entry entry : result14.entrySet()) {
            System.out.println(entry.getKey() + ", " + entry.getValue());
        }
        Map<Integer, Map<String, Integer>> result5 = null;
        result5 = dates.stream()
                .collect(groupingBy((MonitoredData m) -> m.getStart().get(Calendar.HOUR_OF_DAY),
                        groupingBy(MonitoredData::getActivity, Collectors.summingInt(a -> 1))));


        Map<Integer, Integer> collect = result5.entrySet().stream().collect(Collectors.toMap(entry -> entry.getKey(), entry -> sum(entry.getValue())));
        //result5.entrySet().stream().max((entry1, entry2) -> entry1.getValue().get() > entry2.getValue() ? 1 : -1).get().getKey();
        for (Entry entry : collect.entrySet()) {
            System.out.println(entry.getKey() + ", " + entry.getValue());

        }
    }
}
