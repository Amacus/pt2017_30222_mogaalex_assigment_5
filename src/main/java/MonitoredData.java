import java.util.Calendar;
import java.util.List;

/**
 * Created by Amacus5 on 20.05.2017.
 */
public class MonitoredData {
    private Calendar start;
    private Calendar end;
    private String activity;

    public MonitoredData(Calendar start, Calendar end, String activity) {
        this.start = start;
        this.end = end;
        this.activity = activity;
    }

    public Calendar getStart() {
        return start;
    }

    public void setStart(Calendar start) {
        this.start = start;
    }

    public Calendar getEnd() {
        return end;
    }

    public void setEnd(Calendar end) {
        this.end = end;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }
    public int gettIndex(List<MonitoredData> l, MonitoredData x){
        return l.indexOf(x);
    }
}
