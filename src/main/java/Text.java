import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by Amacus5 on 20.05.2017.
 */
public class Text {
    List<String> lines = new ArrayList<String>();
    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public SimpleDateFormat getFormat1() {
        return format1;
    }

    public void getText(Collection<MonitoredData> dates) {

        /*try {
            s = new Scanner(new File("Activities.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }*/
         String fileName = "Activities.txt";
        //read file into stream, try-with-resources
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.forEach(s->{String[] split = s.split("\t\t");
                try {
                    Calendar cal = Calendar.getInstance();
                    Calendar cal2 = Calendar.getInstance();
                    cal.setTime(format1.parse(split[0]));
                    cal2.setTime(format1.parse(split[1]));
                    dates.add(new MonitoredData(cal,cal2,split[2]));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(MonitoredData d:dates){
            System.out.println(format1.format(d.getStart().getTime()) +"\t\t"+format1.format(d.getEnd().getTime())+"\t\t"+d.getActivity());
        }
    }
    public void setText(String s){
        lines.add(s);
        try {
            Files.write(Paths.get("out.txt"), lines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
